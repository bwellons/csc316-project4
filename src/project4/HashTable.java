package project4;

import java.util.ArrayList;

public class HashTable {
	/*
	 * Can implement with:
	 * Linked Lists, Binary Search Tress O(n)
	 * Balanced Search Trees O(log n)
	 */
	HashItem hashTable[];
	int arraySize;
	
	HashTable(int size) {
		hashTable = new HashItem[size];
		
		for (int i = 0; i < arraySize; i++) {
			hashTable[i] = null;
		}
		
		arraySize = size;
	}	
	
	public boolean lookup(String s) {
		int hashIndex = HashCode(s);
		
		HashItem current = hashTable[hashIndex];
		
		while (current != null) {
			if (current.getValue().equals(s))
				return true;
			current = current.getNext();
		}
		
		return false;
	}
	
	public void insert(String value) {
		int hashCode = HashCode(value);
		
		HashItem current = hashTable[hashCode];
		
		if (current == null) {
			hashTable[hashCode] = new HashItem(value, null);
			return;
		}
		
		while(current != null && current.getNext() != null) {
			current = current.getNext();
		}
		current.setNext(new HashItem(value, null));
	}
	
	public int HashCode(String input) {
		int h = 0; 
		for (int i = 0; i < input.length(); i++) {
			h += (h << 5) | (h >>> 27);
			h += (int) input.charAt(i);
		}
		
		return Math.abs(h) % arraySize;
	}
}

class HashItem {
	String value;
	HashItem next;
	
	HashItem(String value, HashItem next) {
		this.value = value;
		this.next = next;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public HashItem getNext() {
		return this.next;
	}
	
	public void setNext(HashItem n) {
		this.next = n;
	}
}
