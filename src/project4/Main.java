package project4;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Main {
	private static HashTable hashTable;
	private static String userFilename;
	
	public static void main(String[] args) throws IOException {
		// Chose 1.5n for hashTable size
		hashTable = new HashTable(37500);
		readDictionaryFile();
		getUserFileName();
		
		
	}
	
	public static void readDictionaryFile() throws IOException {
		String line;
	    try {
			BufferedReader br = new BufferedReader(new FileReader("./dict.txt"));
			while ((line = br.readLine()) != null) {
				hashTable.insert(line.trim());
			}
			br.close();
			System.out.println("Done");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void getUserFileName() {
		Scanner scanner = new Scanner( System.in );
		System.out.println("Enter your file name (it should be located in the root of the project): ");
		userFilename = scanner.next();
		scanner.close();

	}

}
